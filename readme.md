# GNU/Linux Debian en una DELL Precision 3551

La computador Dell Precision 3551 viene con dos controladores de vídeo para la optimización del consumo de energía. Una controladora NVIDIA GP107GLM [Quadro P620] y una Intel UHD.

Según la wiki oficial de GNU/Linux Debian [NVIDIA Optimus](https://wiki.debian.org/NVIDIA%20Optimus), este tipo de controladora se puede instalar con las siguientes técnicas:

- [NVIDIA PRIME Render Offload](https://wiki.debian.org/NVIDIA%20Optimus#Using_NVIDIA_PRIME_Render_Offload)
- [nvidia-xrun](https://wiki.debian.org/NVIDIA%20Optimus#Using_nvidia-xrun)
- [Bumblebee](https://wiki.debian.org/Bumblebee)
- [Nouveau](https://nouveau.freedesktop.org/wiki/Optimus/)
- [only one GPU](https://wiki.debian.org/NVIDIA%20Optimus#Using_only_one_GPU)
- [NVIDIA GPU as the primary GPU](https://wiki.debian.org/NVIDIA%20Optimus#Using_NVIDIA_GPU_as_the_primary_GPU)

Con ninguna de las técnicas anteriores obtuve resultados positivos —con Debian Buster y la controladora NVIDIA GP107GLM—, la única opción que me funcionó, fue incorporar la rama _testing_ a los orígenes de paquetes, para hacer el _upgrade_ a _Bullseye/Sid_.  Los pasos para conseguirlo se detallan a continuación.

## Hace _sudoer_ al usuario regular

Para hacer _sudoer_ al usuario regular, debe iniciar sesión como _root_, ejecutar el siguiente comando y salir

```bash
su
gpasswd -a <user> sudo
```

## Limpieza del sistema

Eliminar cualquier _driver_ previamente instalado

```bash
sudo apt remove --purge nvidia* libnvidia* bumblebee* primus* xserver-xorg-video-* -y
sudo apt-get autoremove
```
<div style="page-break-after: always;"></div>

## Colocar el driver _nouveau_ en lista negra

Se debe colocar el _driver_ **Nouveau** en lista negra, para que no entre en conflicto con el _driver_ privativo de NVIDIA

```bash
cat <<EOF | sudo tee /etc/modprobe.d/nouveau-blacklist.conf
blacklist nouveau
blacklist lbm-nouveau
options nouveau modeset=0
alias nouveau off
alias lbm-nouveau off
EOF
```

Actualizar el _bootloader_ y definir el entorno de texto por defecto

```bash
sudo update-initramfs -u
sudo systemctl set-default multi-user.target
sudo systemctl reboot
```

## Modificar los orígenes de paquetes

Agregar el universo _contrib_ a los orígenes de paquetes

```bash
sudo sed -i '/^deb/s/$/ contrib/' /etc/apt/sources.list
```

Agregar el universo _non-free_ a los orígenes de paquetes

```bash
sudo sed -i '/^deb/s/$/ non-free/' /etc/apt/sources.list
```

Agregar la rama _testing_ a los orígenes de paquetes

```bash
echo "deb http://deb.debian.org/debian/ testing main contrib non-free" | sudo tee -a /etc/apt/sources.list.d/deb-bullseye.list
```

## Verificar los dispositivos de video

Instalar el script _inxi_ para obtener información detallada acerca del hardware de video

```bash
sudo apt-get install inxi
```

Obtener la información del hardware de video antes de instalar los controladores

```bash
inxi -Gx
```

Se debería obtener una salida similar a la siguiente

    Graphics: Device-1: Intel vendor: Dell driver: N/A bus ID: 00:02.0 
              Device-2: NVIDIA vendor: Dell driver: N/A bus ID: 01:00.0 
              Display: tty server: X.org 1.20.4 driver: vesa unloaded: fbdev,modesetting tty: 112x27
              Message: Advanved graphics data unavailable in console. Try -G --display

## Actualizar la distro a la versión SID (Bullseye)

Actualizar las lista de paquetes elegibles

```bash
sudo apt-get update
```

Actualizar la _distro_

```bash
sudo apt-get dist-upgrade
```

## Instalar controladores privativos

Estando en la rama Sid, se procede a instalar el controlador privativo de _nvidia_.

```bash
sudo apt install linux-headers-$(uname -r|sed 's/[^-]*-[^-]*-//') nvidia-driver
```

Instalar el controlador inalámbrico privativo de Intel

```bash
sudo apt install firmware-iwlwifi
sudo modprobe -r iwlwifi ; sudo modprobe iwlwifi
```

Configurar el arranque el modo gráfico multiusuario

```bash
sudo systemctl set-default graphical.target
```
<div style="page-break-after: always;"></div>

Reiniciar el sistema

```bash
sudo systemctl reboot
```

## Verificar los controladores gráficos

Obtener la información del hardware de video luego de la instalación de los controladores de video

```bash
inxi -Gx
```

Se debería obtener una salida similar a la siguiente

    Graphics: 
    Device-1: Intel UHD Graphics vendor: Dell driver: i915 v: kernel bus ID: 00:02.0 
    Device-2: NVIDIA GP107GLM [Quadro P620] vendor: Dell driver: nvidia v: 450.66 bus ID: 01:00.0
    Device-3: Realtek Integrated_Webcam_HD type: USB driver: uvcvideo bus ID: 1-11:3 
    Display: x11 server: X.Org 1.20.8 driver: modesetting,nvidia unloaded: fbdev,nouveau,vesa resolution: 1920x1080~60Hz 
    OpenGL: renderer: Mesa Intel UHD Graphics (CML GT2) v: 4.6 Mesa 20.1.9 direct render: Yes

## Referencias

[Forosla — "Cómo instalar el controlador nvidia en Debian"](https://www.forosla.com/como-instalar-el-controlador-nvidia-en-debian-guia-completa-solucion/)

## Auto-promoción

Si lo desea puede darse una vuelta por mi [canal](http://youtube.com/c/MizaqScreencasts) y seguirme en [Twitter](https://twitter.com/mismatso).


## License

**GNU/Linux Debian en DELL Precision 3551** by *Misael Matamoros* is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-nc-sa/4.0/).

Trabajo basado en [https://gitlab.com/mismatso/debian-on-dell-precision-3551/](https://gitlab.com/mismatso/debian-on-dell-precision-3551/)

![Licencia Creative Commons](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png) 